﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principale
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lb_name = New System.Windows.Forms.Label()
        Me.btn_jouer = New System.Windows.Forms.Button()
        Me.btn_regles = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lb_name
        '
        Me.lb_name.AutoSize = True
        Me.lb_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.25!)
        Me.lb_name.Location = New System.Drawing.Point(57, 9)
        Me.lb_name.Name = "lb_name"
        Me.lb_name.Size = New System.Drawing.Size(160, 47)
        Me.lb_name.TabIndex = 0
        Me.lb_name.Text = "Qwirkle"
        '
        'btn_jouer
        '
        Me.btn_jouer.Location = New System.Drawing.Point(99, 100)
        Me.btn_jouer.Name = "btn_jouer"
        Me.btn_jouer.Size = New System.Drawing.Size(75, 23)
        Me.btn_jouer.TabIndex = 1
        Me.btn_jouer.Text = "Jouer"
        Me.btn_jouer.UseVisualStyleBackColor = True
        '
        'btn_regles
        '
        Me.btn_regles.Location = New System.Drawing.Point(74, 152)
        Me.btn_regles.Name = "btn_regles"
        Me.btn_regles.Size = New System.Drawing.Size(123, 23)
        Me.btn_regles.TabIndex = 2
        Me.btn_regles.Text = "Règles du jeu"
        Me.btn_regles.UseVisualStyleBackColor = True
        '
        'Principale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.btn_regles)
        Me.Controls.Add(Me.btn_jouer)
        Me.Controls.Add(Me.lb_name)
        Me.Name = "Principale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lb_name As Label
    Friend WithEvents btn_jouer As Button
    Friend WithEvents btn_regles As Button
End Class
