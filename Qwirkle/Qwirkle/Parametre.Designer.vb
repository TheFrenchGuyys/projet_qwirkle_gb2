﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Parametre
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_retour = New System.Windows.Forms.Button()
        Me.btn_lancer = New System.Windows.Forms.Button()
        Me.cb_coulj4 = New System.Windows.Forms.ComboBox()
        Me.cb_coulj3 = New System.Windows.Forms.ComboBox()
        Me.cb_coulj2 = New System.Windows.Forms.ComboBox()
        Me.cb_coulj1 = New System.Windows.Forms.ComboBox()
        Me.date_j4 = New System.Windows.Forms.DateTimePicker()
        Me.date_j3 = New System.Windows.Forms.DateTimePicker()
        Me.date_j2 = New System.Windows.Forms.DateTimePicker()
        Me.date_j1 = New System.Windows.Forms.DateTimePicker()
        Me.tb_j4 = New System.Windows.Forms.TextBox()
        Me.tb_j2 = New System.Windows.Forms.TextBox()
        Me.tb_j3 = New System.Windows.Forms.TextBox()
        Me.tb_j1 = New System.Windows.Forms.TextBox()
        Me.lb_nom = New System.Windows.Forms.Label()
        Me.rb_4j = New System.Windows.Forms.RadioButton()
        Me.rb_3j = New System.Windows.Forms.RadioButton()
        Me.rb_2j = New System.Windows.Forms.RadioButton()
        Me.rb_1j = New System.Windows.Forms.RadioButton()
        Me.gb_j1 = New System.Windows.Forms.GroupBox()
        Me.gb_j2 = New System.Windows.Forms.GroupBox()
        Me.gb_j3 = New System.Windows.Forms.GroupBox()
        Me.gb_j4 = New System.Windows.Forms.GroupBox()
        Me.gb_j1.SuspendLayout()
        Me.gb_j2.SuspendLayout()
        Me.gb_j3.SuspendLayout()
        Me.gb_j4.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_retour
        '
        Me.btn_retour.Location = New System.Drawing.Point(1045, 519)
        Me.btn_retour.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_retour.Name = "btn_retour"
        Me.btn_retour.Size = New System.Drawing.Size(100, 28)
        Me.btn_retour.TabIndex = 71
        Me.btn_retour.Text = "Retour"
        Me.btn_retour.UseVisualStyleBackColor = True
        '
        'btn_lancer
        '
        Me.btn_lancer.Location = New System.Drawing.Point(529, 519)
        Me.btn_lancer.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_lancer.Name = "btn_lancer"
        Me.btn_lancer.Size = New System.Drawing.Size(157, 28)
        Me.btn_lancer.TabIndex = 70
        Me.btn_lancer.Text = "Lancer la partie"
        Me.btn_lancer.UseVisualStyleBackColor = True
        '
        'cb_coulj4
        '
        Me.cb_coulj4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cb_coulj4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cb_coulj4.FormattingEnabled = True
        Me.cb_coulj4.Items.AddRange(New Object() {"Rouge", "Bleu", "Violet", "Vert"})
        Me.cb_coulj4.Location = New System.Drawing.Point(8, 89)
        Me.cb_coulj4.Margin = New System.Windows.Forms.Padding(4)
        Me.cb_coulj4.MaxLength = 1
        Me.cb_coulj4.Name = "cb_coulj4"
        Me.cb_coulj4.Size = New System.Drawing.Size(160, 24)
        Me.cb_coulj4.TabIndex = 69
        '
        'cb_coulj3
        '
        Me.cb_coulj3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cb_coulj3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cb_coulj3.FormattingEnabled = True
        Me.cb_coulj3.Items.AddRange(New Object() {"Rouge", "Bleu", "Violet", "Vert"})
        Me.cb_coulj3.Location = New System.Drawing.Point(8, 89)
        Me.cb_coulj3.Margin = New System.Windows.Forms.Padding(4)
        Me.cb_coulj3.MaxLength = 1
        Me.cb_coulj3.Name = "cb_coulj3"
        Me.cb_coulj3.Size = New System.Drawing.Size(160, 24)
        Me.cb_coulj3.TabIndex = 68
        '
        'cb_coulj2
        '
        Me.cb_coulj2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cb_coulj2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cb_coulj2.FormattingEnabled = True
        Me.cb_coulj2.Items.AddRange(New Object() {"Rouge", "Bleu", "Violet", "Vert"})
        Me.cb_coulj2.Location = New System.Drawing.Point(8, 87)
        Me.cb_coulj2.Margin = New System.Windows.Forms.Padding(4)
        Me.cb_coulj2.MaxLength = 1
        Me.cb_coulj2.Name = "cb_coulj2"
        Me.cb_coulj2.Size = New System.Drawing.Size(160, 24)
        Me.cb_coulj2.TabIndex = 67
        '
        'cb_coulj1
        '
        Me.cb_coulj1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cb_coulj1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cb_coulj1.FormattingEnabled = True
        Me.cb_coulj1.Items.AddRange(New Object() {"Rouge", "Bleu", "Violet", "Vert"})
        Me.cb_coulj1.Location = New System.Drawing.Point(8, 87)
        Me.cb_coulj1.Margin = New System.Windows.Forms.Padding(4)
        Me.cb_coulj1.MaxLength = 1
        Me.cb_coulj1.Name = "cb_coulj1"
        Me.cb_coulj1.Size = New System.Drawing.Size(160, 24)
        Me.cb_coulj1.TabIndex = 66
        '
        'date_j4
        '
        Me.date_j4.Location = New System.Drawing.Point(8, 57)
        Me.date_j4.Margin = New System.Windows.Forms.Padding(4)
        Me.date_j4.Name = "date_j4"
        Me.date_j4.Size = New System.Drawing.Size(265, 22)
        Me.date_j4.TabIndex = 65
        Me.date_j4.Value = New Date(2019, 5, 23, 0, 0, 0, 0)
        '
        'date_j3
        '
        Me.date_j3.Location = New System.Drawing.Point(8, 57)
        Me.date_j3.Margin = New System.Windows.Forms.Padding(4)
        Me.date_j3.Name = "date_j3"
        Me.date_j3.Size = New System.Drawing.Size(265, 22)
        Me.date_j3.TabIndex = 64
        Me.date_j3.Value = New Date(2019, 5, 23, 0, 0, 0, 0)
        '
        'date_j2
        '
        Me.date_j2.Location = New System.Drawing.Point(8, 55)
        Me.date_j2.Margin = New System.Windows.Forms.Padding(4)
        Me.date_j2.Name = "date_j2"
        Me.date_j2.Size = New System.Drawing.Size(265, 22)
        Me.date_j2.TabIndex = 63
        Me.date_j2.Value = New Date(2019, 5, 23, 0, 0, 0, 0)
        '
        'date_j1
        '
        Me.date_j1.CustomFormat = ""
<<<<<<< HEAD
        Me.date_j1.Location = New System.Drawing.Point(6, 45)
=======
        Me.date_j1.Location = New System.Drawing.Point(8, 55)
        Me.date_j1.Margin = New System.Windows.Forms.Padding(4)
>>>>>>> b133ab009a470e9ff494585175cf94b4e00cc12f
        Me.date_j1.Name = "date_j1"
        Me.date_j1.Size = New System.Drawing.Size(265, 22)
        Me.date_j1.TabIndex = 62
        Me.date_j1.Value = New Date(2019, 12, 25, 0, 0, 0, 0)
        '
        'tb_j4
        '
        Me.tb_j4.Location = New System.Drawing.Point(8, 25)
        Me.tb_j4.Margin = New System.Windows.Forms.Padding(4)
        Me.tb_j4.MaxLength = 10
        Me.tb_j4.Name = "tb_j4"
        Me.tb_j4.Size = New System.Drawing.Size(132, 22)
        Me.tb_j4.TabIndex = 61
        '
        'tb_j2
        '
        Me.tb_j2.Location = New System.Drawing.Point(8, 23)
        Me.tb_j2.Margin = New System.Windows.Forms.Padding(4)
        Me.tb_j2.MaxLength = 10
        Me.tb_j2.Name = "tb_j2"
        Me.tb_j2.Size = New System.Drawing.Size(132, 22)
        Me.tb_j2.TabIndex = 60
        '
        'tb_j3
        '
        Me.tb_j3.Location = New System.Drawing.Point(8, 25)
        Me.tb_j3.Margin = New System.Windows.Forms.Padding(4)
        Me.tb_j3.MaxLength = 10
        Me.tb_j3.Name = "tb_j3"
        Me.tb_j3.Size = New System.Drawing.Size(132, 22)
        Me.tb_j3.TabIndex = 59
        '
        'tb_j1
        '
        Me.tb_j1.Location = New System.Drawing.Point(8, 23)
        Me.tb_j1.Margin = New System.Windows.Forms.Padding(4)
        Me.tb_j1.MaxLength = 10
        Me.tb_j1.Name = "tb_j1"
        Me.tb_j1.Size = New System.Drawing.Size(132, 22)
        Me.tb_j1.TabIndex = 58
        Me.tb_j1.Text = "TEST"
        '
        'lb_nom
        '
        Me.lb_nom.AutoSize = True
        Me.lb_nom.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.25!)
        Me.lb_nom.Location = New System.Drawing.Point(425, 18)
        Me.lb_nom.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lb_nom.Name = "lb_nom"
        Me.lb_nom.Size = New System.Drawing.Size(323, 95)
        Me.lb_nom.TabIndex = 53
        Me.lb_nom.Text = "Qwirkle"
        '
        'rb_4j
        '
        Me.rb_4j.AutoSize = True
        Me.rb_4j.Location = New System.Drawing.Point(545, 210)
        Me.rb_4j.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_4j.Name = "rb_4j"
        Me.rb_4j.Size = New System.Drawing.Size(92, 21)
        Me.rb_4j.TabIndex = 52
        Me.rb_4j.Text = "4 Joueurs"
        Me.rb_4j.UseVisualStyleBackColor = True
        '
        'rb_3j
        '
        Me.rb_3j.AutoSize = True
        Me.rb_3j.Location = New System.Drawing.Point(545, 182)
        Me.rb_3j.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_3j.Name = "rb_3j"
        Me.rb_3j.Size = New System.Drawing.Size(92, 21)
        Me.rb_3j.TabIndex = 51
        Me.rb_3j.Text = "3 Joueurs"
        Me.rb_3j.UseVisualStyleBackColor = True
        '
        'rb_2j
        '
        Me.rb_2j.AutoSize = True
        Me.rb_2j.Location = New System.Drawing.Point(545, 154)
        Me.rb_2j.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_2j.Name = "rb_2j"
        Me.rb_2j.Size = New System.Drawing.Size(92, 21)
        Me.rb_2j.TabIndex = 50
        Me.rb_2j.Text = "2 Joueurs"
        Me.rb_2j.UseVisualStyleBackColor = True
        '
        'rb_1j
        '
        Me.rb_1j.AutoSize = True
        Me.rb_1j.Location = New System.Drawing.Point(545, 126)
        Me.rb_1j.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_1j.Name = "rb_1j"
        Me.rb_1j.Size = New System.Drawing.Size(168, 21)
        Me.rb_1j.TabIndex = 49
        Me.rb_1j.Text = "1 Joueur ( pas dispo )"
        Me.rb_1j.UseVisualStyleBackColor = True
        '
        'gb_j1
        '
        Me.gb_j1.Controls.Add(Me.tb_j1)
        Me.gb_j1.Controls.Add(Me.date_j1)
        Me.gb_j1.Controls.Add(Me.cb_coulj1)
        Me.gb_j1.Location = New System.Drawing.Point(129, 239)
        Me.gb_j1.Margin = New System.Windows.Forms.Padding(4)
        Me.gb_j1.Name = "gb_j1"
        Me.gb_j1.Padding = New System.Windows.Forms.Padding(4)
        Me.gb_j1.Size = New System.Drawing.Size(293, 123)
        Me.gb_j1.TabIndex = 72
        Me.gb_j1.TabStop = False
        Me.gb_j1.Text = "Joueur 1"
        '
        'gb_j2
        '
        Me.gb_j2.Controls.Add(Me.tb_j2)
        Me.gb_j2.Controls.Add(Me.date_j2)
        Me.gb_j2.Controls.Add(Me.cb_coulj2)
        Me.gb_j2.Location = New System.Drawing.Point(731, 239)
        Me.gb_j2.Margin = New System.Windows.Forms.Padding(4)
        Me.gb_j2.Name = "gb_j2"
        Me.gb_j2.Padding = New System.Windows.Forms.Padding(4)
        Me.gb_j2.Size = New System.Drawing.Size(283, 123)
        Me.gb_j2.TabIndex = 73
        Me.gb_j2.TabStop = False
        Me.gb_j2.Text = "Joueur 2"
        '
        'gb_j3
        '
        Me.gb_j3.Controls.Add(Me.date_j3)
        Me.gb_j3.Controls.Add(Me.tb_j3)
        Me.gb_j3.Controls.Add(Me.cb_coulj3)
        Me.gb_j3.Location = New System.Drawing.Point(129, 384)
        Me.gb_j3.Margin = New System.Windows.Forms.Padding(4)
        Me.gb_j3.Name = "gb_j3"
        Me.gb_j3.Padding = New System.Windows.Forms.Padding(4)
        Me.gb_j3.Size = New System.Drawing.Size(293, 123)
        Me.gb_j3.TabIndex = 74
        Me.gb_j3.TabStop = False
        Me.gb_j3.Text = "Joueur 3"
        '
        'gb_j4
        '
        Me.gb_j4.Controls.Add(Me.cb_coulj4)
        Me.gb_j4.Controls.Add(Me.tb_j4)
        Me.gb_j4.Controls.Add(Me.date_j4)
        Me.gb_j4.Location = New System.Drawing.Point(731, 384)
        Me.gb_j4.Margin = New System.Windows.Forms.Padding(4)
        Me.gb_j4.Name = "gb_j4"
        Me.gb_j4.Padding = New System.Windows.Forms.Padding(4)
        Me.gb_j4.Size = New System.Drawing.Size(283, 123)
        Me.gb_j4.TabIndex = 75
        Me.gb_j4.TabStop = False
        Me.gb_j4.Text = "Joueur 4"
        '
        'Parametre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1271, 567)
        Me.Controls.Add(Me.gb_j4)
        Me.Controls.Add(Me.gb_j3)
        Me.Controls.Add(Me.gb_j2)
        Me.Controls.Add(Me.gb_j1)
        Me.Controls.Add(Me.btn_retour)
        Me.Controls.Add(Me.btn_lancer)
        Me.Controls.Add(Me.lb_nom)
        Me.Controls.Add(Me.rb_4j)
        Me.Controls.Add(Me.rb_3j)
        Me.Controls.Add(Me.rb_2j)
        Me.Controls.Add(Me.rb_1j)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Parametre"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Parametre"
        Me.gb_j1.ResumeLayout(False)
        Me.gb_j1.PerformLayout()
        Me.gb_j2.ResumeLayout(False)
        Me.gb_j2.PerformLayout()
        Me.gb_j3.ResumeLayout(False)
        Me.gb_j3.PerformLayout()
        Me.gb_j4.ResumeLayout(False)
        Me.gb_j4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_retour As Button
    Friend WithEvents btn_lancer As Button
    Friend WithEvents cb_coulj4 As ComboBox
    Friend WithEvents cb_coulj3 As ComboBox
    Friend WithEvents cb_coulj2 As ComboBox
    Friend WithEvents cb_coulj1 As ComboBox
    Friend WithEvents date_j4 As DateTimePicker
    Friend WithEvents date_j3 As DateTimePicker
    Friend WithEvents date_j2 As DateTimePicker
    Friend WithEvents date_j1 As DateTimePicker
    Friend WithEvents tb_j4 As TextBox
    Friend WithEvents tb_j2 As TextBox
    Friend WithEvents tb_j3 As TextBox
    Friend WithEvents tb_j1 As TextBox
    Friend WithEvents lb_nom As Label
    Friend WithEvents rb_4j As RadioButton
    Friend WithEvents rb_3j As RadioButton
    Friend WithEvents rb_2j As RadioButton
    Friend WithEvents rb_1j As RadioButton
    Friend WithEvents gb_j1 As GroupBox
    Friend WithEvents gb_j2 As GroupBox
    Friend WithEvents gb_j3 As GroupBox
    Friend WithEvents gb_j4 As GroupBox
End Class
