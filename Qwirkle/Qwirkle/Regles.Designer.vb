﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Regles
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Regles))
        Me.btn_menu1 = New System.Windows.Forms.Button()
        Me.pb_regle1 = New System.Windows.Forms.PictureBox()
        Me.btn_suivant1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.pb_regle1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_menu1
        '
        Me.btn_menu1.Location = New System.Drawing.Point(280, 682)
        Me.btn_menu1.Name = "btn_menu1"
        Me.btn_menu1.Size = New System.Drawing.Size(75, 23)
        Me.btn_menu1.TabIndex = 0
        Me.btn_menu1.Text = "Menu"
        Me.btn_menu1.UseVisualStyleBackColor = True
        '
        'pb_regle1
        '
        Me.pb_regle1.BackgroundImage = CType(resources.GetObject("pb_regle1.BackgroundImage"), System.Drawing.Image)
        Me.pb_regle1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pb_regle1.Location = New System.Drawing.Point(0, -1)
        Me.pb_regle1.Name = "pb_regle1"
        Me.pb_regle1.Size = New System.Drawing.Size(634, 655)
        Me.pb_regle1.TabIndex = 2
        Me.pb_regle1.TabStop = False
        '
        'btn_suivant1
        '
        Me.btn_suivant1.Location = New System.Drawing.Point(551, 660)
        Me.btn_suivant1.Name = "btn_suivant1"
        Me.btn_suivant1.Size = New System.Drawing.Size(75, 23)
        Me.btn_suivant1.TabIndex = 3
        Me.btn_suivant1.Text = "Suivant"
        Me.btn_suivant1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(295, 660)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Page 1"
        '
        'Regles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(638, 717)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_suivant1)
        Me.Controls.Add(Me.pb_regle1)
        Me.Controls.Add(Me.btn_menu1)
        Me.MaximumSize = New System.Drawing.Size(654, 755)
        Me.MinimumSize = New System.Drawing.Size(654, 755)
        Me.Name = "Regles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regles"
        CType(Me.pb_regle1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_menu1 As Button
    Friend WithEvents pb_regle1 As PictureBox
    Friend WithEvents btn_suivant1 As Button
    Friend WithEvents Label1 As Label
End Class
