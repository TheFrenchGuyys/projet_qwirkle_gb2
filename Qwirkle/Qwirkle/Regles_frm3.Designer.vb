﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Regles_frm3
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Regles_frm3))
        Me.pb_regle3 = New System.Windows.Forms.PictureBox()
        Me.btn_precedent3 = New System.Windows.Forms.Button()
        Me.btn_menu3 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.pb_regle3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pb_regle3
        '
        Me.pb_regle3.BackgroundImage = CType(resources.GetObject("pb_regle3.BackgroundImage"), System.Drawing.Image)
        Me.pb_regle3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pb_regle3.Location = New System.Drawing.Point(2, 3)
        Me.pb_regle3.Name = "pb_regle3"
        Me.pb_regle3.Size = New System.Drawing.Size(634, 655)
        Me.pb_regle3.TabIndex = 3
        Me.pb_regle3.TabStop = False
        '
        'btn_precedent3
        '
        Me.btn_precedent3.Location = New System.Drawing.Point(10, 660)
        Me.btn_precedent3.Name = "btn_precedent3"
        Me.btn_precedent3.Size = New System.Drawing.Size(75, 23)
        Me.btn_precedent3.TabIndex = 9
        Me.btn_precedent3.Text = "Précédent"
        Me.btn_precedent3.UseVisualStyleBackColor = True
        '
        'btn_menu3
        '
        Me.btn_menu3.Location = New System.Drawing.Point(278, 682)
        Me.btn_menu3.Name = "btn_menu3"
        Me.btn_menu3.Size = New System.Drawing.Size(75, 23)
        Me.btn_menu3.TabIndex = 7
        Me.btn_menu3.Text = "Menu"
        Me.btn_menu3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(293, 661)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Page 3"
        '
        'Regles_frm3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 717)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_precedent3)
        Me.Controls.Add(Me.btn_menu3)
        Me.Controls.Add(Me.pb_regle3)
        Me.MaximumSize = New System.Drawing.Size(654, 755)
        Me.MinimumSize = New System.Drawing.Size(654, 755)
        Me.Name = "Regles_frm3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regles_frm3"
        CType(Me.pb_regle3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pb_regle3 As PictureBox
    Friend WithEvents btn_precedent3 As Button
    Friend WithEvents btn_menu3 As Button
    Friend WithEvents Label1 As Label
End Class
