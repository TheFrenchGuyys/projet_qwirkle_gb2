﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Regles_frm2
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Regles_frm2))
        Me.btn_suivant2 = New System.Windows.Forms.Button()
        Me.btn_menu2 = New System.Windows.Forms.Button()
        Me.btn_precedent2 = New System.Windows.Forms.Button()
        Me.pb_regle2 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.pb_regle2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_suivant2
        '
        Me.btn_suivant2.Location = New System.Drawing.Point(551, 663)
        Me.btn_suivant2.Name = "btn_suivant2"
        Me.btn_suivant2.Size = New System.Drawing.Size(75, 23)
        Me.btn_suivant2.TabIndex = 5
        Me.btn_suivant2.Text = "Suivant"
        Me.btn_suivant2.UseVisualStyleBackColor = True
        '
        'btn_menu2
        '
        Me.btn_menu2.Location = New System.Drawing.Point(280, 685)
        Me.btn_menu2.Name = "btn_menu2"
        Me.btn_menu2.Size = New System.Drawing.Size(75, 23)
        Me.btn_menu2.TabIndex = 4
        Me.btn_menu2.Text = "Menu"
        Me.btn_menu2.UseVisualStyleBackColor = True
        '
        'btn_precedent2
        '
        Me.btn_precedent2.Location = New System.Drawing.Point(12, 663)
        Me.btn_precedent2.Name = "btn_precedent2"
        Me.btn_precedent2.Size = New System.Drawing.Size(75, 23)
        Me.btn_precedent2.TabIndex = 6
        Me.btn_precedent2.Text = "Précédent"
        Me.btn_precedent2.UseVisualStyleBackColor = True
        '
        'pb_regle2
        '
        Me.pb_regle2.BackgroundImage = CType(resources.GetObject("pb_regle2.BackgroundImage"), System.Drawing.Image)
        Me.pb_regle2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pb_regle2.Location = New System.Drawing.Point(3, 2)
        Me.pb_regle2.Name = "pb_regle2"
        Me.pb_regle2.Size = New System.Drawing.Size(634, 655)
        Me.pb_regle2.TabIndex = 7
        Me.pb_regle2.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(299, 663)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Page 2"
        '
        'Regles_frm2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 717)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pb_regle2)
        Me.Controls.Add(Me.btn_precedent2)
        Me.Controls.Add(Me.btn_suivant2)
        Me.Controls.Add(Me.btn_menu2)
        Me.MaximumSize = New System.Drawing.Size(654, 755)
        Me.MinimumSize = New System.Drawing.Size(654, 755)
        Me.Name = "Regles_frm2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regles_frm2"
        CType(Me.pb_regle2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_suivant2 As Button
    Friend WithEvents btn_menu2 As Button
    Friend WithEvents btn_precedent2 As Button
    Friend WithEvents pb_regle2 As PictureBox
    Friend WithEvents Label1 As Label
End Class
